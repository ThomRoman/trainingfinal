package com.trainingFinal.trainingFinal.repository;


import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.model.WarehouseType;
import com.trainingFinal.trainingFinal.repository.impl.WarehouseTypeRepositoryImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WarehouseTypeRepositoryTest {

	@Mock
	private JdbcTemplate jdbcTemplate;

	@Mock
	private SimpleJdbcCall simpleJdbcCall;

	@InjectMocks
	private WarehouseTypeRepositoryImpl warehouseTypeRepository;

	@BeforeAll
	public void setUp() {

		jdbcTemplate = mock(JdbcTemplate.class);
		simpleJdbcCall = mock(SimpleJdbcCall.class);

		given(simpleJdbcCall.withProcedureName(anyString())).willReturn(simpleJdbcCall);
		given(simpleJdbcCall.declareParameters(any(SqlParameter.class),any(SqlOutParameter.class),any(SqlOutParameter.class))).willReturn(simpleJdbcCall);

		warehouseTypeRepository = new WarehouseTypeRepositoryImpl(jdbcTemplate, simpleJdbcCall);
	}

	@BeforeEach
	public void each() {
		given(simpleJdbcCall.withProcedureName(anyString())).willReturn(simpleJdbcCall);
		given(simpleJdbcCall.declareParameters(any(SqlParameter.class),any(SqlOutParameter.class),any(SqlOutParameter.class))).willReturn(simpleJdbcCall);


		warehouseTypeRepository = new WarehouseTypeRepositoryImpl(jdbcTemplate, simpleJdbcCall);
	}

	@Test
	public void getWarehouseTypesTest() {
		ArrayList<WarehouseType> list = new ArrayList(List.of(new WarehouseType(1,"bebidas")));
		given(jdbcTemplate.query(anyString(), any(RowMapper.class))).willReturn(list);
		var response = warehouseTypeRepository.getWarehouseTypes();
		assertEquals(list,response);

	}
}
