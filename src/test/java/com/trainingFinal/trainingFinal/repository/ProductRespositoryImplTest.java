package com.trainingFinal.trainingFinal.repository;


import com.trainingFinal.trainingFinal.dto.ProductDTO;
import com.trainingFinal.trainingFinal.model.Product;
import com.trainingFinal.trainingFinal.repository.impl.ProductRepositoryImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductRespositoryImplTest {

	@Mock
	private JdbcTemplate jdbcTemplate;

	@Mock
	private SimpleJdbcInsert simpleJdbcInsert;

	@InjectMocks
	private ProductRepositoryImpl productRepositoryImpl;

	@BeforeAll
	public void setUp() {

		jdbcTemplate = mock(JdbcTemplate.class);
		simpleJdbcInsert = mock(SimpleJdbcInsert.class);
		given(simpleJdbcInsert.withTableName(anyString())).willReturn(simpleJdbcInsert);

		productRepositoryImpl = new ProductRepositoryImpl(jdbcTemplate, simpleJdbcInsert);
	}

	@Test
	public void getProductsTest(){
		ArrayList<Product> list = new ArrayList(List.of(new Product(),new Product(),new Product()));
		given(jdbcTemplate.query(anyString(), any(RowMapper.class))).willReturn(list);
		var response = productRepositoryImpl.getProducts();
		assertEquals(list,response);
		assertEquals(list.size(),response.size());
	}

	@Test
	public void mapToProductTest() throws SQLException {
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getInt("id")).thenReturn(1);
		when(resultSet.getString("name")).thenReturn("Casco");
		when(resultSet.getString("sku")).thenReturn("codigo01");
		when(resultSet.getString("partNumber")).thenReturn("H");
		when(resultSet.getDouble("cost")).thenReturn(10.0);
		when(resultSet.getInt("totalStock")).thenReturn(10);
		when(resultSet.getInt("idProductType")).thenReturn(1);

		Product pr = productRepositoryImpl.mapToProduct(resultSet);
		assertNotNull(pr);
		assertEquals(10.0,pr.getCost());
		assertEquals(1,pr.getId());
		assertEquals("Casco",pr.getName());
		assertEquals("codigo01",pr.getSku());
		assertEquals("H",pr.getPartNumber());
		assertEquals(10,pr.getTotalStock());
		assertEquals(1,pr.getProductType().getId());
	}

	@Test
	public void getProductByIdTest(){
		String sql = "SELECT * FROM Product WHERE id = ?";
		var pr = new Product();
		pr.setName("Casco");
		pr.setSku("codigo01");
		given(jdbcTemplate.queryForObject(eq(sql), any(RowMapper.class), eq(1))).willReturn(pr);
		var p = productRepositoryImpl.getProductById(1);
		assertEquals(pr.getId(), p.getId());
		assertEquals(pr.getSku(), p.getSku());
	}

}
