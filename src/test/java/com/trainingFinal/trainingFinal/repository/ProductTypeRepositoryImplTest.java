package com.trainingFinal.trainingFinal.repository;


import com.mysql.cj.xdevapi.Client;
import com.trainingFinal.trainingFinal.dto.ProductTypeDTO;
import com.trainingFinal.trainingFinal.model.Product;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.repository.impl.ProductTypeRepositoryImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTransientException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductTypeRepositoryImplTest {

	@Mock
	private JdbcTemplate jdbcTemplate;

	@Mock
	private SimpleJdbcCall simpleJdbcCall;

	@InjectMocks
	private ProductTypeRepositoryImpl productTypeRepositoryImpl;

	@BeforeAll
	public void setUp() {

		jdbcTemplate = mock(JdbcTemplate.class);
		simpleJdbcCall = mock(SimpleJdbcCall.class);
		given(simpleJdbcCall.withProcedureName(anyString())).willReturn(simpleJdbcCall);
		given(simpleJdbcCall.declareParameters(any(SqlParameter.class),any(SqlOutParameter.class),any(SqlOutParameter.class))).willReturn(simpleJdbcCall);

		productTypeRepositoryImpl = new ProductTypeRepositoryImpl(jdbcTemplate, simpleJdbcCall);


	}

	@BeforeEach
	public void each(){
		given(simpleJdbcCall.withProcedureName(anyString())).willReturn(simpleJdbcCall);
		given(simpleJdbcCall.declareParameters(any(SqlParameter.class),any(SqlOutParameter.class),any(SqlOutParameter.class))).willReturn(simpleJdbcCall);

		productTypeRepositoryImpl = new ProductTypeRepositoryImpl(jdbcTemplate, simpleJdbcCall);
	}


	@Test
	public void createProductTypeTest() {
		ProductType expected = new ProductType(1,"Bebidas");
		ProductTypeDTO productTypeDTO = new ProductTypeDTO("Bebidas");

		Map<String, Object> outParams = new HashMap<>();
		outParams.put("out_id","1");
		outParams.put("out_name","Bebidas");
		given(simpleJdbcCall.execute(any(Map.class))).willReturn(outParams);

		ProductType productTypeResponse = productTypeRepositoryImpl.createProductType(productTypeDTO);
		assertNotNull(productTypeResponse);
		assertEquals(expected.getId(), productTypeResponse.getId());
		assertEquals(expected.getName(), productTypeResponse.getName());
	}

	@Test
	public void createProductWithErrorTest(){
		ProductTypeDTO productTypeDTO = new ProductTypeDTO("Bebidas");
		given(simpleJdbcCall.execute(any(Map.class))).willThrow(new RuntimeException("error"));
		assertThrows(NullPointerException.class, () -> {
			productTypeRepositoryImpl.createProductType(productTypeDTO);
		});
	}

	@Test
	public void getProductTypesTest() {
		ArrayList<ProductType> list = new ArrayList(List.of(new ProductType(1,"bebidas")));
		given(jdbcTemplate.query(anyString(), any(RowMapper.class))).willReturn(list);
		var response = productTypeRepositoryImpl.getProductTypes();
		assertEquals(list,response);

	}

	@Test
	void mapToProductTypeTest() throws SQLException {
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getInt("id")).thenReturn(1);
		when(resultSet.getString("name")).thenReturn("bebidas");
		ProductType pt = productTypeRepositoryImpl.mapToProductType(resultSet);

		assertNotNull(pt);
		assertEquals(1, pt.getId());
		assertEquals("bebidas", pt.getName());


	}

	@Test
	void getByIdTest(){
		var pt = new ProductType(1,"bebidas");
		String sql = "SELECT * FROM ProductType WHERE id = ?";
		given(jdbcTemplate.queryForObject(eq(sql), any(RowMapper.class), eq(1))).willReturn(pt);

		ProductType fetchedProductType = productTypeRepositoryImpl.getProductTypeById(1);


		assertEquals(pt.getId(), fetchedProductType.getId());
		assertEquals(pt.getName(), fetchedProductType.getName());

	}

}
