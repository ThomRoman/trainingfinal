package com.trainingFinal.trainingFinal.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.trainingFinal.trainingFinal.model.Product;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.service.ProductTypeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductTypeController.class)
public class ProductTypeControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ProductTypeService productTypeService;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void getProductsTest() throws Exception {
		ArrayList<ProductType> productTypes = new ArrayList<>(List.of(new ProductType(1,"tipo 1")));
		given(productTypeService.getProductTypes()).willReturn(productTypes);

		ResultActions response = mockMvc.perform(get("/v1/productTypes"));
		response.andExpect(status().isOk())
				.andDo(print())
				.andExpect(jsonPath("$.size()",is(productTypes.size())));

	}

	@Test
	public void getAllError() throws Exception{
		int expectedStatus = 500;
		String uri = "/v1/productTypes";
		when(productTypeService.getProductTypes()).thenThrow(new NullPointerException("Error"));
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
		int actualStatus = mvcResult.getResponse().getStatus();
		assertEquals(expectedStatus, actualStatus);

	}

	@Test
	public void getById() throws  Exception{
		ProductType pt = new ProductType();
		pt.setId(1);
		pt.setName("casco");
		given(productTypeService.getProductTypeById(1)).willReturn(pt);

		ResultActions response = mockMvc.perform(get("/v1/productTypes/{id}",1));
		response.andExpect(status().isOk())
				.andDo(print())
				.andExpect(jsonPath("$.name",is(pt.getName())))
				.andExpect(jsonPath("$.id",is(pt.getId())));
	}

	@Test
	public void getByIdError() throws Exception{
		int expectedStatus = 500;
		String uri = "/v1/productTypes/5";
		when(productTypeService.getProductTypeById(anyInt())).thenThrow(new NullPointerException("Error"));
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
		int actualStatus = mvcResult.getResponse().getStatus();
		assertEquals(expectedStatus, actualStatus);
	}
}
