package com.trainingFinal.trainingFinal.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.trainingFinal.trainingFinal.model.Product;
import com.trainingFinal.trainingFinal.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.CoreMatchers.is;

import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(ProductController.class)
public class ProductControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ProductService productService;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void getProductsTest() throws Exception {
		Product pr = new Product();
		pr.setId(1);
		pr.setName("casco");
		pr.setCost(10.0);
		pr.setSku("codigo001");
		ArrayList<Product> products = new ArrayList<>(List.of(pr));
		given(productService.getProducts()).willReturn(products);

		ResultActions response = mockMvc.perform(get("/v1/products"));
		response.andExpect(status().isOk())
				.andDo(print())
				.andExpect(jsonPath("$.size()",is(products.size())));

	}

	@Test
	public void getAllError() throws Exception{
		int expectedStatus = 500;
		String uri = "/v1/products";
		when(productService.getProducts()).thenThrow(new NullPointerException("Error"));
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
		int actualStatus = mvcResult.getResponse().getStatus();
		assertEquals(expectedStatus, actualStatus);

	}

	@Test
	public void getById() throws  Exception{
		Product pr = new Product();
		pr.setId(1);
		pr.setName("casco");
		pr.setCost(10.0);
		pr.setSku("codigo001");
		given(productService.getProductById(1)).willReturn(pr);

		ResultActions response = mockMvc.perform(get("/v1/products/{id}",1));
		response.andExpect(status().isOk())
				.andDo(print())
				.andExpect(jsonPath("$.name",is(pr.getName())))
				.andExpect(jsonPath("$.sku",is(pr.getSku())));
	}

	@Test
	public void getByIdError() throws Exception{
		int expectedStatus = 500;
		String uri = "/v1/products/5";
		when(productService.getProductById(anyInt())).thenThrow(new NullPointerException("Error"));
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
		int actualStatus = mvcResult.getResponse().getStatus();
		assertEquals(expectedStatus, actualStatus);
	}



}
