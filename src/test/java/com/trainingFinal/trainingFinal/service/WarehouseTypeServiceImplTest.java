package com.trainingFinal.trainingFinal.service;

import com.trainingFinal.trainingFinal.dto.ProductTypeDTO;
import com.trainingFinal.trainingFinal.dto.WarehouseTypeDTO;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.model.WarehouseType;
import com.trainingFinal.trainingFinal.repository.WarehouseTypeRepository;
import com.trainingFinal.trainingFinal.service.impl.WarehouseTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class WarehouseTypeServiceImplTest {
	@Mock
	private WarehouseTypeRepository wareTypeRepository;

	@InjectMocks
	private WarehouseTypeServiceImpl warehouseTypeService;

	@Test
	public void getWarehouseTypesTest(){
		WarehouseType wt = new WarehouseType(1,"wh1");
		ArrayList<WarehouseType> whs = new ArrayList<>();
		whs.add(wt);
		given(wareTypeRepository.getWarehouseTypes()).willReturn(whs);
		var response = warehouseTypeService.getWarehouseTypes();

		assertEquals(whs.size(),response.size());
		assertEquals(whs,response);
	}

	@Test
	public void getWarehouseTypeByIdTest(){
		WarehouseType wt = new WarehouseType(1,"wh1");
		given(wareTypeRepository.getWarehouseTypeById(anyInt())).willReturn(wt);
		var response = warehouseTypeService.getWarehouseTypeById(1);
		assertEquals(wt,response);
	}

	@Test
	public void createProductTypeTest(){
		WarehouseType wt = new WarehouseType(1,"wh1");

		given(wareTypeRepository.createWarehouseType(any(WarehouseTypeDTO.class))).willReturn(wt);
		var response = warehouseTypeService.createWarehouseType(new WarehouseTypeDTO());
		assertEquals(wt,response);
	}
}
