package com.trainingFinal.trainingFinal.service;


import com.trainingFinal.trainingFinal.dto.ProductTypeDTO;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.repository.ProductTypeRepository;
import com.trainingFinal.trainingFinal.service.impl.ProductTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class ProductTypeServiceImplTest {

	@Mock
	private ProductTypeRepository productTypeRepository;

	@InjectMocks
	private ProductTypeServiceImpl productTypeServiceImpl;

	@Test
	public void getProductTypesTest(){
		ArrayList<ProductType> pts = new ArrayList<>();
		given(productTypeRepository.getProductTypes()).willReturn(pts);
		var response = productTypeServiceImpl.getProductTypes();
		assertEquals(pts.size(),response.size());
		assertEquals(pts,response);
	}

	@Test
	public void getProductTypeByIdTest(){
		ProductType pt = new ProductType();
		pt.setId(1);
		pt.setName("Name");
		given(productTypeRepository.getProductTypeById(1)).willReturn(pt);
		var response = productTypeServiceImpl.getProductTypeById(1);
		assertEquals(pt,response);
	}

	@Test
	public void createProductTypeTest(){
		ProductType pt = new ProductType();
		pt.setId(1);
		pt.setName("Name");

		given(productTypeRepository.createProductType(any(ProductTypeDTO.class))).willReturn(pt);
		var response = productTypeServiceImpl.createProductType(new ProductTypeDTO());
		assertEquals(pt,response);
	}
}
