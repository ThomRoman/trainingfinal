package com.trainingFinal.trainingFinal.repository;

import com.trainingFinal.trainingFinal.dto.ProductDTO;
import com.trainingFinal.trainingFinal.dto.ProductTypeDTO;
import com.trainingFinal.trainingFinal.model.Product;
import com.trainingFinal.trainingFinal.model.ProductType;

import java.util.ArrayList;

public interface ProductRepository {
	ArrayList<Product> getProducts();
	Product createProduct(ProductDTO productDTO,int totalStock);
	Product getProductById(int id);
}
