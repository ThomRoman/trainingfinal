package com.trainingFinal.trainingFinal.repository;

import com.trainingFinal.trainingFinal.dto.WarehouseTypeDTO;
import com.trainingFinal.trainingFinal.model.WarehouseType;

import java.util.ArrayList;

public interface WarehouseTypeRepository {
	ArrayList<WarehouseType> getWarehouseTypes();
	WarehouseType createWarehouseType(WarehouseTypeDTO warehouseTypeDTO);
	WarehouseType getWarehouseTypeById(int id);
}
