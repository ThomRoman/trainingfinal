package com.trainingFinal.trainingFinal.repository;

import com.trainingFinal.trainingFinal.dto.ProductTypeDTO;
import com.trainingFinal.trainingFinal.model.ProductType;

import java.util.ArrayList;

public interface ProductTypeRepository {
	ArrayList<ProductType> getProductTypes();
	ProductType createProductType(ProductTypeDTO productTypeDTO);

	ProductType getProductTypeById(int id);
}
