package com.trainingFinal.trainingFinal.repository;

import com.trainingFinal.trainingFinal.dto.WarehouseDTO;
import com.trainingFinal.trainingFinal.model.Warehouse;

import java.util.ArrayList;

public interface WarehouseRepository {
	ArrayList<Warehouse> getWarehouses();
	Warehouse getWarehouseById(int id);
	Warehouse createWarehouse(WarehouseDTO warehouseDTO);
}
