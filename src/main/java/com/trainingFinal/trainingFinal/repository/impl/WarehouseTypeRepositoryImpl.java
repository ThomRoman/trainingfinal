package com.trainingFinal.trainingFinal.repository.impl;

import com.trainingFinal.trainingFinal.dto.WarehouseTypeDTO;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.model.WarehouseType;
import com.trainingFinal.trainingFinal.repository.WarehouseTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class WarehouseTypeRepositoryImpl implements WarehouseTypeRepository {

	private JdbcTemplate jdbcTemplate;

	private SimpleJdbcCall simpleJdbcCall;

	@Autowired
	public WarehouseTypeRepositoryImpl(JdbcTemplate jdbcTemplate,SimpleJdbcCall simpleJdbcCall){
		this.jdbcTemplate = jdbcTemplate;
		this.simpleJdbcCall = simpleJdbcCall.withProcedureName("SP_WAREHOUSE_TYPE")
				.declareParameters(
						new SqlParameter("wt_name", Types.VARCHAR),
						new SqlOutParameter("out_id", Types.INTEGER),
						new SqlOutParameter("out_name", Types.VARCHAR)
				);
	}

	@Override
	public ArrayList<WarehouseType> getWarehouseTypes() {
		String sql = "SELECT * FROM WarehouseType";
		List<WarehouseType> warehouseTypes = jdbcTemplate.query(sql,(rs, rowNum) -> mapToWarehouseType(rs));
		return new ArrayList<>(warehouseTypes);
	}

	public WarehouseType mapToWarehouseType(ResultSet rs) throws SQLException {
		return new WarehouseType(rs.getInt("id"),rs.getString("name"));
	}

	@Override
	public WarehouseType createWarehouseType(WarehouseTypeDTO warehouseTypeDTO) {
		try {
			this.simpleJdbcCall = simpleJdbcCall.withProcedureName("SP_WAREHOUSE_TYPE")
					.declareParameters(
							new SqlParameter("wt_name", Types.VARCHAR),
							new SqlOutParameter("out_id", Types.INTEGER),
							new SqlOutParameter("out_name", Types.VARCHAR)
					);
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("wt_name", warehouseTypeDTO.getName());
			Map<String, Object> outParams = this.simpleJdbcCall.execute(parameters);
			WarehouseType newWarehouseType = new WarehouseType();
			newWarehouseType.setId(Integer.parseInt(outParams.get("out_id").toString()));
			newWarehouseType.setName((String) outParams.get("out_name"));
			return newWarehouseType;
		}catch (Exception e){
			throw new NullPointerException("Error mientras se creaba un WarehouseType");
		}
	}

	@Override
	public WarehouseType getWarehouseTypeById(int id) {
		String sql = "SELECT * FROM WarehouseType WHERE id = ?";
		WarehouseType warehouseType = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToWarehouseType(rs), id);
		return warehouseType;
	}
}
