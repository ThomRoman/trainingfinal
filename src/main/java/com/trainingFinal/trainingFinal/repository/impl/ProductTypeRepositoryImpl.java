package com.trainingFinal.trainingFinal.repository.impl;

import com.trainingFinal.trainingFinal.dto.ProductTypeDTO;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.repository.ProductTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProductTypeRepositoryImpl implements ProductTypeRepository {

	private final Logger log = LoggerFactory.getLogger(ProductTypeRepositoryImpl.class);
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall simpleJdbcCall;
	@Autowired
	public ProductTypeRepositoryImpl(JdbcTemplate jdbcTemplate,SimpleJdbcCall simpleJdbcCall){
		this.jdbcTemplate = jdbcTemplate;
		this.simpleJdbcCall = simpleJdbcCall.withProcedureName("SP_CREATE_PRODUCT_TYPE")
				.declareParameters(
						new SqlParameter("pt_name", Types.VARCHAR),
						new SqlOutParameter("out_id", Types.INTEGER),
						new SqlOutParameter("out_name", Types.VARCHAR)
				);
	}

	@Override
	public ArrayList<ProductType> getProductTypes() {
		String sql = "SELECT * FROM ProductType";
		List<ProductType> productTypeList = jdbcTemplate.query(sql,(rs, rowNum) -> mapToProductType(rs));
		return new ArrayList<>(productTypeList);
	}

	public ProductType mapToProductType(ResultSet rs) throws SQLException {
		return new ProductType(rs.getInt("id"),rs.getString("name"));
	}


	@Override
	public ProductType createProductType(ProductTypeDTO productTypeDTO) {
		try {
			simpleJdbcCall = simpleJdbcCall.withProcedureName("SP_CREATE_PRODUCT_TYPE")
					.declareParameters(
							new SqlParameter("pt_name", Types.VARCHAR),
							new SqlOutParameter("out_id", Types.INTEGER),
							new SqlOutParameter("out_name", Types.VARCHAR)
					);

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("pt_name", productTypeDTO.getName());

			Map<String, Object> outParams = simpleJdbcCall.execute(parameters);
			ProductType newProductType = new ProductType();
			newProductType.setId(Integer.parseInt(outParams.get("out_id").toString()));
			newProductType.setName((String)outParams.get("out_name"));
			return newProductType;
		}catch (Exception e){
			log.info(e.getMessage());
			throw new NullPointerException("Error mientras se creaba un product type");
		}

	}

	@Override
	public ProductType getProductTypeById(int id) {
		String sql = "SELECT * FROM ProductType WHERE id = ?";
		ProductType productType = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToProductType(rs), id);
		return productType;

	}
}
