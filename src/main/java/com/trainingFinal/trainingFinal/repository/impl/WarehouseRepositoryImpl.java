package com.trainingFinal.trainingFinal.repository.impl;

import com.trainingFinal.trainingFinal.dto.WarehouseDTO;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.model.Warehouse;
import com.trainingFinal.trainingFinal.model.WarehouseType;
import com.trainingFinal.trainingFinal.repository.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Repository
public class WarehouseRepositoryImpl implements WarehouseRepository {


	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;

	private final Logger log = LoggerFactory.getLogger(WarehouseRepositoryImpl.class);

	@Autowired
	public WarehouseRepositoryImpl(JdbcTemplate jdbcTemplate,SimpleJdbcInsert simpleJdbcInsert){
		this.jdbcTemplate = jdbcTemplate;
		this.simpleJdbcInsert = simpleJdbcInsert.withTableName("Warehouse").usingGeneratedKeyColumns("id");
	}

	@Override
	public ArrayList<Warehouse> getWarehouses() {
		String sql = "SELECT * FROM Warehouse";
		List<Warehouse> warehouseList = jdbcTemplate.query(sql,(rs, rowNum) -> mapToWarehouse(rs));
		return new ArrayList<>(warehouseList);
	}

	public Warehouse mapToWarehouse(ResultSet rs) throws SQLException {
		var warehouse =  new Warehouse();
		warehouse.setName(rs.getString("name"));
		warehouse.setId(rs.getInt("id"));
		var wt = new WarehouseType();
		wt.setId(rs.getInt("idWarehouseType"));
		warehouse.setWarehouseType(wt);
		return warehouse;
	}

	@Override
	public Warehouse getWarehouseById(int id) {
		String sql = "SELECT * FROM Warehouse WHERE id = ?";
		Warehouse warehouse = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToWarehouse(rs), id);
		return warehouse;
	}

	@Override
	public Warehouse createWarehouse(WarehouseDTO warehouseDTO) {
		try {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("name", warehouseDTO.getName());
			parameters.put("idWarehouseType", warehouseDTO.getIdWarehouseType());
			Warehouse warehouse = new Warehouse();
			int id = simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
			warehouse.setId(id);
			warehouse.setName(warehouseDTO.getName());
			WarehouseType warehouseType = new WarehouseType();
			warehouseType.setId(warehouseDTO.getIdWarehouseType());
			warehouse.setWarehouseType(warehouseType);
			return warehouse;

		}catch (Exception e){
			log.info(e.getMessage());
			throw new NullPointerException("Error mientras se creaba un product type");
		}
	}
}
