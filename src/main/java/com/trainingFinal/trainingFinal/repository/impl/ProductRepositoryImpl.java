package com.trainingFinal.trainingFinal.repository.impl;

import com.trainingFinal.trainingFinal.dto.ProductDTO;
import com.trainingFinal.trainingFinal.model.Product;
import com.trainingFinal.trainingFinal.model.ProductType;

import com.trainingFinal.trainingFinal.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;

	private final Logger log = LoggerFactory.getLogger(ProductRepositoryImpl.class);

	@Autowired
	public ProductRepositoryImpl(JdbcTemplate jdbcTemplate,SimpleJdbcInsert simpleJdbcInsert){
		this.jdbcTemplate = jdbcTemplate;
		this.simpleJdbcInsert = simpleJdbcInsert.withTableName("Product").usingGeneratedKeyColumns("id");
	}
	@Override
	public ArrayList<Product> getProducts() {
		String sql = "SELECT * FROM Product";
		List<Product> productList = jdbcTemplate.query(sql,(rs, rowNum) -> mapToProduct(rs));
		return new ArrayList<>(productList);
	}

	public Product mapToProduct(ResultSet rs) throws SQLException {

		var product = new Product();
		product.setId(rs.getInt("id"));
		product.setName(rs.getString("name"));
		product.setSku(rs.getString("sku"));
		product.setPartNumber(rs.getString("partNumber"));
		product.setCost(rs.getDouble("cost"));
		product.setTotalStock(rs.getInt("totalStock"));

		ProductType productType = new ProductType();
		productType.setId(rs.getInt("idProductType"));

		product.setProductType(productType);

		return product;
	}

	@Override
	public Product createProduct(ProductDTO productDTO,int totalStock) {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("idProductType",productDTO.getIdProductType());
		parameters.put("name",productDTO.getName());
		parameters.put("sku",productDTO.getSku());
		parameters.put("partNumber",productDTO.getPartNumber());
		parameters.put("cost",productDTO.getCost());
		parameters.put("totalStock",totalStock);

		int id = this.simpleJdbcInsert.executeAndReturnKey(parameters).intValue();

		Product product = new Product();
		product.setId(id);
		product.setName(productDTO.getName());
		product.setSku(productDTO.getSku());
		product.setPartNumber(productDTO.getPartNumber());
		product.setCost(productDTO.getCost());
		product.setTotalStock(totalStock);
		return product;
	}

	@Override
	public Product getProductById(int id) {
		String sql = "SELECT * FROM Product WHERE id = ?";
		Product product = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToProduct(rs), id);
		return product;
	}
}
