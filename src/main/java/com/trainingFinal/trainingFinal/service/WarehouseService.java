package com.trainingFinal.trainingFinal.service;

import com.trainingFinal.trainingFinal.dto.WarehouseDTO;
import com.trainingFinal.trainingFinal.model.Warehouse;

import java.util.ArrayList;

public interface WarehouseService {
	ArrayList<Warehouse> getWarehouses();
	Warehouse getWarehouseById(int id);
	Warehouse createWarehouse(WarehouseDTO warehouseDTO);
}
