package com.trainingFinal.trainingFinal.service.impl;

import com.trainingFinal.trainingFinal.dto.WarehouseDTO;
import com.trainingFinal.trainingFinal.model.Warehouse;
import com.trainingFinal.trainingFinal.repository.WarehouseRepository;
import com.trainingFinal.trainingFinal.service.WarehouseService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


@Service
@NoArgsConstructor
@AllArgsConstructor
public class WarehouseServiceImpl implements WarehouseService {


	@Autowired
	private WarehouseRepository warehouseRepository;


	@Override
	public ArrayList<Warehouse> getWarehouses() {
		return warehouseRepository.getWarehouses();
	}

	@Override
	public Warehouse getWarehouseById(int id) {
		return warehouseRepository.getWarehouseById(id);
	}

	@Override
	public Warehouse createWarehouse(WarehouseDTO warehouseDTO) {
		return warehouseRepository.createWarehouse(warehouseDTO);
	}
}
