package com.trainingFinal.trainingFinal.service.impl;

import com.trainingFinal.trainingFinal.dto.ProductDTO;
import com.trainingFinal.trainingFinal.dto.WarehouseDTO;
import com.trainingFinal.trainingFinal.dto.WarehouseXProductDTO;
import com.trainingFinal.trainingFinal.model.Product;
import com.trainingFinal.trainingFinal.model.Warehouse;
import com.trainingFinal.trainingFinal.repository.ProductRepository;
import com.trainingFinal.trainingFinal.repository.ProductTypeRepository;
import com.trainingFinal.trainingFinal.repository.WarehouseRepository;
import com.trainingFinal.trainingFinal.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {


	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductTypeRepository productTypeRepository;

	@Autowired
	private WarehouseRepository warehouseRepository;

	@Override
	public ArrayList<Product> getProducts() {
		var products = productRepository.getProducts();
//		for(int i =0;i<products.size();i++){
//			var typeId = products.get(i).getProductType().getId();
//			var type = productTypeRepository.getProductTypeById(typeId);
//			products.get(i).setProductType(type);
//		}
		return products;
	}

	@Override
	public Product createProduct(ProductDTO productDTO) {
		try{
			int totalStock = 0;
			for(WarehouseXProductDTO warehouseXProductDTO: productDTO.getWarehousesDTO()){
				Warehouse warehouse = warehouseRepository.getWarehouseById(warehouseXProductDTO.getIdWarehouse());
				totalStock+= warehouseXProductDTO.getStock();
			}
			var type = productTypeRepository.getProductTypeById(productDTO.getIdProductType());

			var productCreated =  productRepository.createProduct(productDTO,totalStock);
			productCreated.setProductType(type);
			return productCreated;
		}catch (Exception ex){
			throw  ex;
		}

	}

	@Override
	public Product getProductById(int id) {
		var product =  productRepository.getProductById(id);
		var type = productTypeRepository.getProductTypeById(product.getProductType().getId());
		product.setProductType(type);
		return product;
	}
}
