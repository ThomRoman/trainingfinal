package com.trainingFinal.trainingFinal.service.impl;

import com.trainingFinal.trainingFinal.dto.ProductTypeDTO;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.repository.ProductTypeRepository;
import com.trainingFinal.trainingFinal.service.ProductTypeService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProductTypeServiceImpl implements ProductTypeService {

	@Autowired
	private ProductTypeRepository productTypeRepository;

	@Override
	public ArrayList<ProductType> getProductTypes() {
		return productTypeRepository.getProductTypes();
	}

	@Override
	public ProductType createProductType(ProductTypeDTO productTypeDTO) {
		return productTypeRepository.createProductType(productTypeDTO);
	}

	@Override
	public ProductType getProductTypeById(int id) {
		return productTypeRepository.getProductTypeById(id);
	}
}
