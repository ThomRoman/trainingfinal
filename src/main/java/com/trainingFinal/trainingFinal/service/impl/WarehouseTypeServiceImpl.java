package com.trainingFinal.trainingFinal.service.impl;

import com.trainingFinal.trainingFinal.dto.WarehouseTypeDTO;
import com.trainingFinal.trainingFinal.model.WarehouseType;
import com.trainingFinal.trainingFinal.repository.WarehouseTypeRepository;
import com.trainingFinal.trainingFinal.service.WarehouseTypeService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class WarehouseTypeServiceImpl implements WarehouseTypeService {

	@Autowired
	private WarehouseTypeRepository warehouseTypeRepository;

	@Override
	public ArrayList<WarehouseType> getWarehouseTypes() {
		return warehouseTypeRepository.getWarehouseTypes();
	}

	@Override
	public WarehouseType createWarehouseType(WarehouseTypeDTO warehouseTypeDTO) {
		return warehouseTypeRepository.createWarehouseType(warehouseTypeDTO);
	}

	@Override
	public WarehouseType getWarehouseTypeById(int id) {
		return warehouseTypeRepository.getWarehouseTypeById(id);
	}
}
