package com.trainingFinal.trainingFinal.service;

import com.trainingFinal.trainingFinal.dto.ProductDTO;
import com.trainingFinal.trainingFinal.model.Product;

import java.util.ArrayList;

public interface ProductService {
	ArrayList<Product> getProducts();
	Product createProduct(ProductDTO productDTO);
	Product getProductById(int id);
}
