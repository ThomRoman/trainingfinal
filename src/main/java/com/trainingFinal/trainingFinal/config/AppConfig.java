package com.trainingFinal.trainingFinal.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;

@Configuration
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AppConfig {

	@Autowired
	private DataSource dataSource;

	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(dataSource);
	}

	@Bean
	public SimpleJdbcInsert simpleJdbcInsert() {
		return new SimpleJdbcInsert(jdbcTemplate());
	}

	@Bean
	public SimpleJdbcCall simpleJdbcCall(){
		return new SimpleJdbcCall(jdbcTemplate());
	}
}
