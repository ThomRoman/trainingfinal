package com.trainingFinal.trainingFinal.controller;

import com.trainingFinal.trainingFinal.dto.WarehouseDTO;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.model.Warehouse;
import com.trainingFinal.trainingFinal.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/warehouses")
public class WarehouseController {

	@Autowired
	private WarehouseService warehouseService;


	@GetMapping("")
	public ResponseEntity<List<Warehouse>> getWarehouses(){
		try {
			List<Warehouse> warehouseList = warehouseService.getWarehouses();
			return ResponseEntity.status(HttpStatus.OK).body(warehouseList);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}


	@GetMapping("/{id}")
	public  ResponseEntity<Warehouse> getWarehouseById(@PathVariable(name = "id") Integer id){
		try {
			Warehouse warehouse = warehouseService.getWarehouseById(id);
			return ResponseEntity.status(HttpStatus.OK).body(warehouse);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping("")
	@Transactional
	public ResponseEntity<Warehouse> createWarehouse(@RequestBody WarehouseDTO warehouseDTO){
		try {
			Warehouse warehouse = warehouseService.createWarehouse(warehouseDTO);
			return ResponseEntity.status(HttpStatus.CREATED).body(warehouse);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

}
