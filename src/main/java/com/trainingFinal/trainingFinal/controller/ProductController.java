package com.trainingFinal.trainingFinal.controller;

import com.trainingFinal.trainingFinal.dto.ProductDTO;
import com.trainingFinal.trainingFinal.model.Product;
import com.trainingFinal.trainingFinal.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/products")
public class ProductController {

	@Autowired
	private ProductService productService;


	@GetMapping("")
	public ResponseEntity<List<Product>> getProducts(){
		try {
			List<Product> productList = productService.getProducts();
			return ResponseEntity.status(HttpStatus.OK).body(productList);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}

	}

	@PostMapping("")
	@Transactional
	public ResponseEntity<Product> createProduct(@RequestBody ProductDTO productDTO){
		try {
			Product product = productService.createProduct(productDTO);
			return ResponseEntity.status(HttpStatus.CREATED).body(product);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}

	}

	@GetMapping("/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable(name = "id") Integer id){
		try {
			Product product = productService.getProductById(id);
			return ResponseEntity.status(HttpStatus.OK).body(product);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}

	}
}
