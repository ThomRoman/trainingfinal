package com.trainingFinal.trainingFinal.controller;

import com.trainingFinal.trainingFinal.dto.ProductTypeDTO;
import com.trainingFinal.trainingFinal.model.ProductType;
import com.trainingFinal.trainingFinal.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/productTypes")
public class ProductTypeController {

	@Autowired
	private ProductTypeService productTypeService;


	@GetMapping("")
	public ResponseEntity<List<ProductType>> getProductTypes(){
		try {
			List<ProductType> productTypeList = productTypeService.getProductTypes();
			return ResponseEntity.status(HttpStatus.OK).body(productTypeList);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}

	}

	@PostMapping("")
	@Transactional
	public ResponseEntity<ProductType> createProductType(@RequestBody ProductTypeDTO productTypeDTO){
		try {
			ProductType productType = productTypeService.createProductType(productTypeDTO);
			return ResponseEntity.status(HttpStatus.CREATED).body(productType);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}

	}

	@GetMapping("/{id}")
	public ResponseEntity<ProductType> getProductTypeById(@PathVariable(name = "id") Integer id){
		try {
			ProductType productType = productTypeService.getProductTypeById(id);
			return ResponseEntity.status(HttpStatus.OK).body(productType);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}

	}
}
