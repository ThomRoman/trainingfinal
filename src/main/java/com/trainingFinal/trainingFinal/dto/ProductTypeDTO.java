package com.trainingFinal.trainingFinal.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductTypeDTO {
	private String name;
}
