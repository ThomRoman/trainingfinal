package com.trainingFinal.trainingFinal.dto;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseTypeDTO {
	private String name;
}
