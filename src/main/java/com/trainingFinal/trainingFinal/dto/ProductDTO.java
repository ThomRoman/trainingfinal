package com.trainingFinal.trainingFinal.dto;

import com.trainingFinal.trainingFinal.model.WarehouseXProduct;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {
	private int idProductType;
	private String name;
	private String sku;
	private String partNumber;

	private double cost;
	private ArrayList<WarehouseXProductDTO> warehousesDTO;
}
