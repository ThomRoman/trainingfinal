package com.trainingFinal.trainingFinal.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseXProduct {
	private int id;
	private Warehouse warehouse;
	private Product product;
	private int stock;
}
