package com.trainingFinal.trainingFinal.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Warehouse {
	private int id;
	private String name;
	private WarehouseType warehouseType;
}
