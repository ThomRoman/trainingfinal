package com.trainingFinal.trainingFinal.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {
	private int id;
	private String name;
	private String sku;
	private String partNumber;

	private double cost;
	private int totalStock;

	private ProductType productType;
}
