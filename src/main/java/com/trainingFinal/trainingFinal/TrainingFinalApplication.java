package com.trainingFinal.trainingFinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingFinalApplication.class, args);
	}

}
