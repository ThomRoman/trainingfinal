CREATE DATABASE IF NOT EXISTS trainingFinal;

USE trainingFinal;

CREATE TABLE IF NOT EXISTS WarehouseType(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS ProductType(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS Warehouse(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    idWarehouseType INT UNSIGNED,
     name VARCHAR(255),
    FOREIGN KEY(idWarehouseType) REFERENCES WarehouseType(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS Product(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    idProductType INT UNSIGNED,
    name VARCHAR(255),
    sku VARCHAR(255),
    partNumber VARCHAR(255),
    cost DECIMAL(19,2),
    totalStock INT,
    FOREIGN KEY(idProductType) REFERENCES ProductType(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS WarehouseXProduct(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    idWarehouse INT UNSIGNED,
    idProduct INT UNSIGNED,
    stock INT,
    FOREIGN KEY(idWarehouse) REFERENCES Warehouse(id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY(idProduct) REFERENCES Product(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

DROP PROCEDURE IF EXISTS SP_CREATE_PRODUCT_TYPE;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE SP_CREATE_PRODUCT_TYPE (
    IN pt_name VARCHAR(255),
    OUT out_id INT,
    OUT out_name VARCHAR(255)
)
BEGIN
    DECLARE productTypeExists INT;

    SELECT COUNT(*)
    INTO productTypeExists
    FROM ProductType pt
    WHERE pt.name = pt_name ;

    IF productTypeExists > 0 THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'El product Type ya existe';
    END IF;

    INSERT INTO ProductType (name)
    VALUES(pt_name);

    SET out_id = LAST_INSERT_ID();
    SET out_name = pt_name;

END$$

DROP PROCEDURE IF EXISTS SP_WAREHOUSE_TYPE;
CREATE DEFINER=`root`@`localhost` PROCEDURE SP_WAREHOUSE_TYPE (
    IN wt_name VARCHAR(255),
    OUT out_id INT,
    OUT out_name VARCHAR(255)
)
BEGIN
    DECLARE wtExists INT;

    SELECT COUNT(*)
    INTO wtExists
    FROM WarehouseType wt
    WHERE wt.name = wt_name ;

    IF wtExists > 0 THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'El WarehouseType ya existe';
    END IF;

    INSERT INTO WarehouseType (name)
    VALUES(wt_name);

    SET out_id = LAST_INSERT_ID();
    SET out_name = wt_name;
END$$

DROP PROCEDURE IF EXISTS SP_CREATE_WAREHOUSE;
CREATE DEFINER=`root`@`localhost` PROCEDURE SP_CREATE_WAREHOUSE (
    IN wname VARCHAR(255),
    IN w_id_wt INT,
    OUT out_name VARCHAR(255),
    OUT out_id INT,
    OUT out_id_wt INT
)
BEGIN
    DECLARE wExists INT;
    DECLARE wt_exists INT;

    SELECT COUNT(*)
    INTO wExists
    FROM Warehouse w
    WHERE w.name = wname ;

    IF wExists > 0 THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'El WarehouseT ya existe';
    END IF;

    SELECT COUNT(*)
    INTO wt_exists
    FROM WarehouseType
    WHERE id = w_id_wt;

    IF wExists = 0 THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'El WarehouseType no ya existe';
    END IF;

    INSERT INTO Warehouse (name,idWarehouseType)
    VALUES(wname,w_id_wt);

    SET out_id = LAST_INSERT_ID();
    SET out_name = wname;
    SET out_id_wt = w_id_wt;
END$$
DELIMITER ;
